<?php

use \TDR\WordPress\UserDataManagement\UserDataManagement;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.thedrawingroomcreative.com/
 * @since             1.0.0
 * @package           TDR\WordPress\UserDataManagement
 *
 * @wordpress-plugin
 * Plugin Name:       TDR User Data Management
 * Plugin URI:        https://www.thedrawingroomcreative.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            The Drawing Room
 * Author URI:        https://www.thedrawingroomcreative.com/
 * License:           Proprietary
 * Text Domain:       tdr-user-data-management
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if (! class_exists(TDR\WordPress\UserDataManagement\Loader::class)) {
    spl_autoload_register(function ($class) {
        if (strpos($class, 'TDR\WordPress\UserDataManagement') !== 0) {
            return;
        }
        $path = substr($class, 33);
        include_once(__DIR__ . '/src/' . $path . '.php');
    });
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'TDR_USER_DATA_MANAGEMENT_VERSION', '1.0.0' );

define( 'TDR_USER_DATA_MANAGEMENT_DIR', plugin_dir_path( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tdr-user-data-management-activator.php
 */
function activate_tdr_udm() {
    \TDR\WordPress\UserDataManagement\Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tdr-user-data-management-deactivator.php
 */
function deactivate_tdr_udm() {
    \TDR\WordPress\UserDataManagement\Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tdr_udm' );
register_deactivation_hook( __FILE__, 'deactivate_tdr_udm' );


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function tdr_udm_run() {
    if( function_exists('acf_add_local_field_group') ):
        require_once(TDR_USER_DATA_MANAGEMENT_DIR . '/acf.php');
    endif;
    UserDataManagement::get_instance()->run();
}

function tdr_udm_cookie_message() {
    UserDataManagement::get_instance()->show_message();
}

function tdr_udm_get_cookie_status() {
    UserDataManagement::get_instance()->get_cookie_status();
}

tdr_udm_run();