<?php

namespace TDR\WordPress\UserDataManagement;

/**
 * Fired during plugin activation
 *
 * @link       https://www.thedrawingroomcreative.com/
 * @since      1.0.0
 *
 * @package    TDR_USER_DATA_MANAGEMENT
 * @subpackage TDR_USER_DATA_MANAGEMENT/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    TDR_USER_DATA_MANAGEMENT
 * @subpackage TDR_USER_DATA_MANAGEMENT/includes
 * @author     The Drawing Room <hello@thedrawingroomcreative.com>
 */
class Activator {

    const DB_VERSION = '1.0.0';

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
        self::check_dependencies();
        self::create_db_table();
	}

	public static function check_dependencies() {
        if ( ! is_plugin_active('advanced-custom-fields-pro/acf.php') && current_user_can( 'activate_plugins' ) ) {
            wp_die('The TDR Cookie Management plugin requires Advanced Custom Fields Pro to be installed and active. Please install and activate the plugin before attempting to reactivate the TDR Cookie Management plugin.');
        }
    }

    public static function create_db_table() {
        global $wpdb;

        $table_name = $wpdb->prefix . 'user_data_log';
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "
        CREATE TABLE $table_name (
            ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
            ip_address varchar(39) DEFAULT '0',
            acceptance tinyint(2) NOT NULL DEFAULT '0',
            time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY  (ID)
) $charset_collate";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);

        add_option('tdr_udl_db_version', self::DB_VERSION);
    }

}
