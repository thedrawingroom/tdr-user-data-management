<?php

namespace TDR\WordPress\UserDataManagement;

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.thedrawingroomcreative.com/
 * @since      1.0.0
 *
 * @package    TDR_USER_DATA_MANAGEMENT
 * @subpackage TDR_USER_DATA_MANAGEMENT/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    TDR_USER_DATA_MANAGEMENT
 * @subpackage TDR_USER_DATA_MANAGEMENT/includes
 * @author     The Drawing Room <hello@thedrawingroomcreative.com>
 */
class i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'tdr-user-data-management',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
