<?php

namespace TDR\WordPress\UserDataManagement;

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.thedrawingroomcreative.com/
 * @since      1.0.0
 *
 * @package    TDR_USER_DATA_MANAGEMENT
 * @subpackage TDR_USER_DATA_MANAGEMENT/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    TDR_USER_DATA_MANAGEMENT
 * @subpackage TDR_USER_DATA_MANAGEMENT/includes
 * @author     The Drawing Room <hello@thedrawingroomcreative.com>
 */
class Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
