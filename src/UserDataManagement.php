<?php

namespace TDR\WordPress\UserDataManagement;

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.thedrawingroomcreative.com/
 * @since      1.0.0
 *
 * @package    TDR\WordPress\UserDataManagement
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    TDR\WordPress\UserDataManagement
 * @author     The Drawing Room <hello@thedrawingroomcreative.com>
 */
class UserDataManagement {

    /**
     * The single instance of this class.
     *
     * @since    1.0.0
     * @access   protected
     * @var      UserDataManagement
     */
    protected static $instance;

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

    /**
     * Gets plugin assets.
     *
     * @since    1.0.0
     * @access   protected
     * @var      JsonManifest    $assets
     */
    protected $assets;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {
		if ( defined( 'TDR_USER_DATA_MANAGEMENT_VERSION' ) ) {
			$this->version = TDR_USER_DATA_MANAGEMENT_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'tdr-user-data-management';

        $this->loader = new Loader();
        $this->assets = new JsonManifest( TDR_USER_DATA_MANAGEMENT_DIR . '/public/assets.json', plugin_dir_url(TDR_USER_DATA_MANAGEMENT_DIR . '/index.php' ) . 'public' );

        $this->init();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

    /**
     * Main CookieManagement Instance
     *
     * Ensures only one instance of CookieManagement is loaded or can be loaded.
     *
     * @return UserDataManagement
     */
    public static function get_instance() {
	    if (!isset(self::$instance)) {
	        self::$instance = new self;
        }
        return self::$instance;
    }

    private function init() {
	    acf_add_options_sub_page([
	        'page_title' => 'User Data Management',
            'menu_title' => 'User Data Management',
            'parent_slug' => 'options-general.php'
        ]);
    }

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the TDR_USER_DATA_MANAGEMENT_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

        $this->loader->add_action( 'wp', $this, 'analytics' );
        $this->loader->add_action( 'wp', $this, 'output' );
        $this->loader->add_action( 'wp_enqueue_scripts', $this, 'enqueue_public_assets', 8 );
	    $this->loader->add_action( 'wp_ajax_tdr_user_data_log', $this, 'store_audit_record' );
	    $this->loader->add_action( 'wp_ajax_nopriv_tdr_user_data_log', $this, 'store_audit_record' );

	    $this->loader->add_filter( 'tdr/user-data-management/message/classes', $this, 'css_class_output', 15 );

	}

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
	public function output() {
        if ( $location = apply_filters( 'tdr/user-data-management/location', false ) ) {
            add_action( $location, [ $this, 'show_message' ] );
        }
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function show_message() {
        if ( ! locate_template( ['tdr-user-data-management/cookie-message.php'], true ) ) {
            require_once( TDR_USER_DATA_MANAGEMENT_DIR . '/templates/cookie-message.php' );
        }
    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

    /**
     * Enqueue the plugin's public assets
     *
     * @since     1.0.0
     * @return    void
     */
    public function enqueue_public_assets() {

        $scripts = apply_filters( 'tdr/user-data-management/scripts', true ) ? $this->assets->getUri( 'scripts/main.js' ) : false;
        $styles = apply_filters( 'tdr/user-data-management/styles', true ) ? $this->assets->getUri( 'styles/main.css' ) : false;

        if ($scripts) {
            wp_enqueue_script( $this->plugin_name, $scripts, array( 'jquery' ), $this->version, true );
            wp_localize_script( $this->plugin_name, 'tdrCookies', [
                'recordEndpoint' => admin_url('admin-ajax.php')
            ] );
        }

        if ($styles) {
            wp_enqueue_style( $this->plugin_name, $styles, array(), $this->version, 'all' );
        }

    }

    /**
     * Function to handle AJAX request for storing user acceptance of cookie/data policy.
     *
     * @since     1.0.0
     * @return    void
     */
    public function store_audit_record() {
        global $wpdb;
        $ip = $_SERVER['REMOTE_ADDR'];
        $acceptance = !empty( $_POST['record_confirm'] ) && intval( $_POST['record_confirm'] ) !== 0 ? 1 : 0;
        $time = date( 'c' );
        $wpdb->insert($wpdb->prefix . 'user_data_log', [
            'ip_address' => $ip,
            'acceptance' => $acceptance,
            'time' => $time
        ]);
        wp_die('', 201);
    }

    /**
     * Get the current cookie policy status for the current user.
     *
     * @since     1.0.0
     * @return    boolean
     */
    public function get_cookie_status() {
        return !empty( $_COOKIE['tdr_cookie_policy'] ) && $_COOKIE['tdr_cookie_policy'] === 1;
    }

    /**
     * Outputs CSS classes applied from filter.
     *
     * @param     array|string $classes   An array or string containing CSS classes.
     * @since     1.0.0
     * @return    string
     */
    public function css_class_output( $classes = [] ) {
        if ( !empty($classes) && is_string( $classes ) ) {
            $classes = explode(' ', trim( $classes ));
        }
        elseif ( empty($classes) ) {
            $classes = [];
        }
        if ( $type = get_field('tdr_udm_cookies_type', 'option') ) {
            $classes[] = 'cookie-message--' . $type;
        }
        $classes = array_filter( $classes, function($class) {
            return !empty( trim( $class ) );
        });
        return !empty( $classes ) ? ' ' . implode( ' ', $classes ) : '';
    }

    /**
     * Handles the configuration of Google Analytics if this is controlled by a recognised plugin
     *
     * @since     1.0.0
     * @return    void
     */
    public function analytics() {
        if ( !$this->get_cookie_status() ) {
            if ( get_field( 'tdr_udm_analytics_disabled' ) ) {
                remove_action('wp_head', 'monsterinsights_tracking_script', 6);
            }
            elseif ( get_field( 'tdr_udm_analytics_anonymise' ) ) {
                add_filter( 'monsterinsights_get_option_anonymize_ips', '__return_true' );
            }
        }
    }

}
