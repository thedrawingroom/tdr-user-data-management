<?php
$confirm = get_field('tdr_udm_cookies_confirmation', 'option');
?>
<div class="cookie-message<?php echo apply_filters( 'tdr/user-data-management/message/classes', '' ); ?> is-hidden js-tdr-cookie-message"<?php if (!empty($confirm['enabled'])) : ?> data-notification-confirm<?php endif; if ($type = get_field('tdr_udm_cookies_type', 'option')) ?> data-notification-type="<?php echo $type; ?>"<?php if (class_exists('woocommerce') && (is_woocommerce() || is_cart() || is_checkout()) && get_field('tdr_udm_cookies_woocommerce_enabled', 'option')) : ?> data-woocommerce-active<?php endif; ?>>
    <div class="cookie-message__inner">
        <div class="cookie-message__main js-tdr-cookie-main">
            <h2 class="cookie-message__title"><?php the_field('tdr_udm_cookies_title', 'option'); ?></h2>
            <div class="cookie-message__content">
                <?php the_field( 'tdr_udm_cookies_content', 'option' ); ?>
            </div>
            <div class="cookie-message__btns">
                <button class="cookie-message__btn cookie-message__btn--accept js-tdr-cookie-accept"><?php _e( ( $text = get_field( 'tdr_udm_cookies_accept_text', 'option' ) ) ? $text : 'Accept Cookies', 'tdr-user-data-management' ); ?></button>
                <button class="cookie-message__btn cookie-message__btn--reject js-tdr-cookie-reject"><?php _e( ( $text = get_field( 'tdr_udm_cookies_reject_text', 'option' ) ) ? $text : 'Reject Cookies', 'tdr-user-data-management' ); ?></button>
            </div>
            <?php if ($disclaimer = get_field( 'tdr_udm_cookies_disclaimer', 'option' )) : ?>
            <div class="cookie-message__disclaimer">
                <?php echo apply_filters( 'the_content', $disclaimer ); ?>
            </div>
            <?php endif; ?>
        </div>
        <?php if (!empty($confirm['enabled'])) : ?>
        <div class="cookie-message__main js-tdr-cookie-confirm is-hidden">
            <h2 class="cookie-message__title"><?php echo !empty($confirm['title']) ? $confirm['title'] : ''; ?></h2>
            <div class="cookie-message__content">
                <?php echo !empty($confirm['content']) ? apply_filters( 'the_content', $confirm['content'] ) : ''; ?>
            </div>
            <div class="cookie-message__btns">
                <button class="cookie-message__btn cookie-message__btn--accept js-tdr-cookie-confirm-cancel"><?php _e( !empty($confirm['cancel_text']) ? $confirm['cancel_text'] : 'Cancel', 'tdr-user-data-management' ); ?></button>
                <button class="cookie-message__btn cookie-message__btn--reject js-tdr-cookie-confirm-reject"><?php _e( !empty($confirm['confirm_text']) ? $confirm['confirm_text'] : 'Yes Opt Out', 'tdr-user-data-management' ); ?></button>
            </div>
            <?php if ($disclaimer = get_field( 'tdr_udm_cookies_disclaimer', 'option' )) : ?>
            <div class="cookie-message__disclaimer">
                <?php echo apply_filters( 'the_content', $disclaimer ); ?>
            </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php if ((class_exists('woocommerce') && $woocmsg = get_field('tdr_udm_cookies_woocommerce', 'option')) && $woocmsg['enabled']) : ?>
<div class="cookie-message cookie-message--woocommerce<?php echo apply_filters( 'tdr/user-data-management/message/classes', '' ); ?> is-hidden js-tdr-cookie-woocommerce-message">
    <div class="cookie-message__inner">
        <div class="cookie-message__main">
            <div class="cookie-message__main js-tdr-cookie-main">
                <h2 class="cookie-message__title"><?php echo !empty($woocmsg['title']) ? $woocmsg['title'] : ''; ?></h2>
                <div class="cookie-message__content">
                    <?php echo !empty($woocmsg['content']) ? apply_filters( 'the_content', $woocmsg['content'] ) : ''; ?>
                </div>
                <div class="cookie-message__btns">
                    <button class="cookie-message__btn cookie-message__btn--accept js-tdr-cookie-accept"><?php _e( $woocmsg['accept_text'] ? $woocmsg['accept_text'] : 'Accept Cookies', 'tdr-user-data-management' ); ?></button>
                    <a href="<?php echo home_url(); ?>" class="cookie-message__btn cookie-message__btn--reject"><?php _e( $woocmsg['reject_text'] ? $woocmsg['reject_text'] : 'Back to Home', 'tdr-user-data-management' ); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
