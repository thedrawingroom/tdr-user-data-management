// import external dependencies
import 'jquery'
import Cookies from 'js-cookie'
import 'events-polyfill/src/constructors/Event'

(function($){
    const $message = $('.js-tdr-cookie-message')
    const cookiePolicy = {
        cookieName: 'tdr_cookie_policy',
        notificationType: $message.attr('data-notification-type'),
        $body: $('body'),
        $message: $message,
        $main: $('.js-tdr-cookie-main'),
        $confirm: $('.js-tdr-cookie-confirm'),
        $woocommerce: $('.js-tdr-cookie-woocommerce-message'),
        $reject: $('.js-tdr-cookie-reject'),
        $accept: $('.js-tdr-cookie-accept'),
        $change: $('.js-tdr-cookie-change'),
        $confirmCancel: $('.js-tdr-cookie-confirm-cancel'),
        $confirmReject: $('.js-tdr-cookie-confirm-reject'),
        cookieEvent: new Event('cookiePolicyChange'),
        confirmEnabled: $message.attr('data-notification-confirm') !== undefined,
        woocommerceEnabled: $message.attr('data-woocommerce-active') !== undefined,
        init() {
            this.triggerEvent()

            if (this.getStatus() === undefined) {
                if (this.woocommerceEnabled) {
                    this.$woocommerce.removeClass('is-hidden').addClass('is-visible')
                }
                else {
                    this.$woocommerce.addClass('is-hidden')
                    this.$confirm.addClass('is-hidden')
                    this.$main.removeClass('is-hidden')
                    this.$message.removeClass('is-hidden').addClass('is-visible')
                }
            }

            this.$accept.on('click', e => {
                this.triggerAccept(e)
            })

            this.$reject.on('click', e => {
                if (this.confirmEnabled) {
                    this.triggerConfirm(e)
                }
                else {
                    this.triggerReject(e)
                }
            })

            this.$confirmCancel.on('click', e => {
                this.triggerConfirm(e)
            })

            this.$confirmReject.on('click', e => {
                this.triggerReject(e)
            })

            this.$change.on('click', e => {
                e.preventDefault()
                this.$main.removeClass('is-hidden')
                this.$confirm.addClass('is-hidden')
                if (this.notificationType === 'banner') {
                    this.$message.slideDown(500, function () {
                        $(window).resize()
                    })
                }
                else {
                    this.$message.removeClass('is-hidden').addClass('is-visible')
                }
            })
        },
        getStatus() {
            return Cookies.get(this.cookieName)
        },
        setStatus(enabled = true, expires = 180) {
            const status = Cookies.set(this.cookieName, enabled ? '1' : '0', { expires: expires })
            this.triggerEvent()
            return status
        },
        triggerEvent() {
            document.dispatchEvent(this.cookieEvent)
        },
        sendRecord() {
            $.ajax(tdrCookies.recordEndpoint, {
                data: {
                    'action': 'tdr_user_data_log',
                    'record_confirm': 1
                },
                method: 'POST'
            })
        },
        triggerAccept(e) {
            e.preventDefault()
            this.setStatus()
            this.sendRecord()
            if (this.notificationType === 'banner') {
                this.$message.slideUp(500, function(){
                    $(window).resize()
                })
            }
            else {
                this.$message.removeClass('is-visible').addClass('is-hidden')
            }
        },
        triggerReject(e) {
            e.preventDefault()
            this.setStatus(false)
            this.triggerEvent()
            if (this.notificationType === 'banner') {
                this.$message.slideUp(500, function(){
                    $(window).resize()
                })
            }
            else {
                this.$message.removeClass('is-visible').addClass('is-hidden')
            }
        },
        triggerConfirm(e) {
            e.preventDefault()
            this.$main.toggleClass('is-hidden')
            this.$confirm.toggleClass('is-hidden')
        }
    }

    window.cookiePolicy = cookiePolicy

    cookiePolicy.init()
})(jQuery)
